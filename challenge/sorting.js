module.exports = {
    quickSort(arr, left, right){
        let pivot,
            partitionIndex;
    
        if(left < right){
            pivot = right;
            partitionIndex = this.partition(arr, pivot, left, right);
    
            this.quickSort(arr, left, partitionIndex - 1);
            this.quickSort(arr, partitionIndex + 1, right);
        }
        return arr;
    },

    partition(arr, pivot, left, right){
        let pivotValue = arr[pivot],
            partitionIndex = left;
    
        for(let i = left; i < right; i++){
            if(arr[i] < pivotValue){
                this.swap(arr, i, partitionIndex);
                partitionIndex++;
            }
        }
        this.swap(arr, right, partitionIndex);
        return partitionIndex;
    },

    swap(arr, i , j){
        let temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}