const nilai = require("./nilai");

module.exports = class hitungNilai extends nilai {
    constructor() {
        super();
    };

    minMax() {
        if(this.array.length == 0) console.log(`\nNilai masih kosong`)
        else {
            let arr = this.array;
            let min = arr[0],
                max = arr[0];
            for(let i = 1; i < arr.length; i++) {
                if(arr[i] < min) {
                    min = arr[i];
                };
                if(arr[i] > max) {
                    max = arr[i];
                };
            };
            console.log(`\nNilai tertinggi : `, max, `\nNilai terendah : `, min);
        }
    };

    avgNilai() {
        if(this.array.length == 0) console.log(`\nNilai masih kosong`)
        else {
            let sum = 0;
            let arr = this.array;
            for(let i = 0; i < arr.length; i++) {
                sum += arr[i];
            };
            let avg = sum / arr.length;
            console.log(`\nNilai rata-rata : `, avg);
        }
    };

    passNotPass() {
        if(this.array.length == 0) console.log(`\nNilai masih kosong`)
        else {
            let pass = 0,
                notPass = 0;
            let arr = this.array;
            for(let i = 0; i < arr.length; i++) {
                if(arr[i] < 75) {
                    notPass++;
                }else {
                    pass++;
                };
            };
            console.log(`\nJumlah siswa lulus : `, pass, `\nJumlah siswa tidak lulus : `, notPass);
        }
    };
};