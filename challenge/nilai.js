const sorting = require("./sorting");

module.exports = class nilai {
    constructor() {
        if(this.constructor === nilai){
            throw new Error("Cannot instantiate from Abstract Class");
        };
        this.array = [];
    };

    inputNilai(value) {
        if (value != "q") {
            this.array.push(+value);
            return true;
        } else {
            return false;
        };
    };

    showNilai() {
        if(this.array.length == 0) console.log(`\nNilai masih kosong`)
        else {
            console.log(sorting.quickSort(this.array, 0, this.array.length-1))
        };
    };
};