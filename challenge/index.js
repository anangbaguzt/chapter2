const nilai = require("./hitungNilai");
const readline = require("readline");
const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

let nilaiSiswa = new nilai;

function main() {
	const question = () => {
		rl.question("\nSilahkan pilih menu di bawah:\n1. Input Nilai\n2. Tampilkan Seluruh Nilai\n3. Nilai Tertinggi dan Terendah\n4. Nilai Rata-Rata\n5. Jumlah Siswa Lulus/Tidak Lulus\n6. Keluar\nPilihan : ", (answer) => {
			answer = +answer;
			switch (answer) {
				case 1:
					console.log("\n");
					inputNilai();
					break;
				case 2:
					nilaiSiswa.showNilai();
					question();
					break;
				case 3:
					nilaiSiswa.minMax();
					question();
					break;
				case 4:
					nilaiSiswa.avgNilai();
					question();
					break;
				case 5:
					nilaiSiswa.passNotPass();
					question();
					break;
				case 6:
					exit();
					break;
				default:
					console.log("Pilihan tidak tersedia!");
					question();
					break;
			};
		});
	};

	question();
};

function exit() {
	rl.close();
};

function inputNilai() {
	rl.question("Masukkan nilai: ", answer => {
		if(nilaiSiswa.inputNilai(answer)) inputNilai();
		else main();
	});
};

rl.on("close", function () {
	console.log("\nKeluar\nTerima kasih, telah menggunakan");
	process.exit(0);
});

main();
